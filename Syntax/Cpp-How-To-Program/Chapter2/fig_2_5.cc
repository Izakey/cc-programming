// Name: Sum.cc
// Purpose: To sum two integers
// Author: Isaac Kamga.

#include <iostream> // preprocessor directive to include the input/output streeam header
using namespace std; // using directive to use any name in the C++ standard library

int main(void)
{
 cout << "Welcome ! This program helps you add two integers. " << endl;
 
 int first_integer, second_integer, sum; // Declaration of the two integers and sum variables

 cout << endl << "Enter the first Integer :";
 cin >> first_integer;

 cout << endl << "Enter the second Integer :";
 cin >> second_integer;

 sum = first_integer + second_integer;

 cout << "The sum of " << first_integer << " and " << second_integer << " is " << sum << " ." << endl << "Bye Bye" << endl;
 return 0;

}
