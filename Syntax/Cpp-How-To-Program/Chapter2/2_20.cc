// Name: Circle.cc
// Purpose: To obtain the diameter, circumference and area of a circle given its radius
// Author: Isaac Kamga.

#include <iostream> // Preprocessor directive to include the input/output stream header
using namespace std; // Using directive to use all names in C++ standard libraries

int main(void)
{
 cout << "Welcome! This program computes the diameter, circumference and area of a circle" << endl;
 float radius, diameter, circumference, area;

 cout << "Please enter a value for the radius of the circle: ";
 cin >> radius;
 cout << endl;

 cout << "The diameter of a circle with radius " << radius << " is " << 2 * radius << "." << endl;
 cout << "The Circumference of a circle with radius " << radius << " is " << 2 * 3.14159 * radius << "." << endl;
 cout << "The area of a circle with with radius " << radius << " is " << 3.14159 * radius * radius << "." << endl;

 return 0;
}
