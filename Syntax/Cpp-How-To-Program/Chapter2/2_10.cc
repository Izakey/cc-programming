// Name: Printing.cc
// Purpose: To print some integers in different ways on the screen
// Author: Isaac Kamga.

#include <iostream> // Preprocessor directive to include the input/output stream header
using namespace std; // using directive to use all names in any C++ standard library

int main(void)
{
 cout << "Welcome! This program prints 4 integers in innovative ways" << endl;
 
 int first_integer, second_integer, third_integer, fourth_integer;

 // cout << first_integer " " second_integer " " third_integer " " fourth_integer;
 cout << first_integer << " " << second_integer << " " << third_integer << " " << fourth_integer << " " << endl;
 cout << first_integer << " ";
 cout << second_integer << " ";
 cout << third_integer << " ";
 cout << fourth_integer << " " << endl;
 
 return 0;
} 

