// Name: Arithmetic.cc
// Purpose: To obtain the sum,difference, product, quotient, average, larger and smaller of two integers
// Author: Isaac Kamga.

#include <iostream> // preprocessor directive to include the input/output streeam header
using namespace std; // using directive to use any name in the C++ standard library

int main(void)
{
 cout << "Welcome! This program helps you add, subtract, multiply and divide two integers." << endl;
 
 int first_integer, second_integer, sum, difference, multiple, quotient, average, smaller, larger; // Declaration of the two integers as well as the sum,difference, multiple, quotient, average variables

 cout << endl << "Enter the first Integer :"; // Prompt user to enter the first integer
 cin >> first_integer;

 cout << endl << "Enter the second Integer :"; // Prompt the user to enter the second integer
 cin >> second_integer;

 sum = first_integer + second_integer;
 difference = ( first_integer >= second_integer ? first_integer - second_integer : second_integer - first_integer );
 smaller = ( first_integer >= second_integer ? second_integer : first_integer );
 larger = ( first_integer <= second_integer ? second_integer : first_integer );
 multiple = first_integer * second_integer;
 quotient = first_integer / second_integer;
 average = ( first_integer + second_integer ) / 2 ;

 // Print out the results of the arithmetic operations.
 cout << "The sum of " << first_integer << " and " << second_integer << " is " << sum << " ." << endl; // Print out the sum 
 cout << "The difference between " << first_integer << " and " << second_integer << " is " << difference << " ." << endl; // Print out the difference
 cout << "The multiple of " << first_integer << " and " << second_integer << " is " << multiple << " ." << endl; // Print out the multiple
 cout << "The quotient of " << first_integer << " and " << second_integer << " is " << quotient << " ." << endl; // pring out the quotient
 cout << "The larger of " << first_integer << " and " << second_integer << " is " << larger << " ." << endl; // Print out the larger
 cout << "The smaller of " << first_integer << " and " << second_integer << " is " << smaller << " ." << endl; // Print out the smaller
 
 return 0;

}
