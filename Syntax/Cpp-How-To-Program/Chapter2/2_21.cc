// Name: 2_21.cc
// Purpose: To print a box, an asterisk, an arrow and a diamond
// Author: Isaac Kamga
// C++ How to Program, 8th Edition by Deitel & Deitel

#include <iostream> // Preprocessor directive to include the input/output header
#include <cstdlib> // Preprocessor directive to include the C standard library
#include <cmath> // Preprocessor directive to include the C math library
using namespace std; // using directive to use all names in standard library

void flip(int m, int n);
void flop(int m, int n);
int main (void)
{
 cout << "Welcome! This program prints the shapes of a box, an asterisk, an arrow and a circle" << endl;
 
 // Declare necessary variables 
 int length, width, radius, i, j;

// Print a Box

 cout << "Okay ! Let's start with the box !" << endl;
 
 cout << "Enter an integer value for the length of the box : ";
 cin >> length;
 
 cout << "Enter an integer value for the width of the box : ";
 cin >> width;
 
 for (i = 0; i < length; i++)
     cout << "* ";
 cout << endl;
 
 for (i = 0; i < width - 2; i++)
     {
 	cout << "* ";
	for (j = 0; j < length - 2; j++)
    	    cout << "  ";
	cout << "*";
	cout << endl;
     }
 for (i = 0; i < length; i++)
     cout << "* ";
 cout << endl;

 // Print an Asterisk
 int aslength, boxlength, wslength, starlength; // Relevant variables
 int println;
 
 cout << "Let's go on to the Asterisk !" << endl;

 cout << "Enter an integer value greater than 1 : ";
 cin >> aslength;

 boxlength = 2 * aslength - 1;
 wslength = aslength - 1;
 starlength = 1;
 println = 0; // We've printed no line yet
 
 for (println = 0; println <= ceil(boxlength / 2); println++)
 { 
	starlength = 2 * ( println ) + 1;
	for (j = 0;j < wslength;j++){
	     cout << "  ";
	}
	for (int k = 0;k < starlength;k++){
	     cout << "* ";
	}
	for (int l = 0;l < wslength;l++){
	     cout << "  ";
	}
	cout << endl;
	if (wslength > 0)
	    wslength--;
 }

 wslength++;

 for (println = ceil(boxlength / 2); println > 0; println--)
  { 
	starlength = 2 * ( println ) - 1;
	for (j = 0;j < wslength;j++){
	     cout << "  ";
	}
	for (int k = 0;k < starlength;k++){
	     cout << "* ";
	}
	for (int l = 0;l < wslength;l++){
	     cout << "  ";
	}
	cout << endl;
	if (wslength >= 0)
	    wslength++;
  }
 
 

 // Print an arrow
 cout << "Let's go on to the Arrow !" << endl;

 cout << "Enter an integer value for an arrow !" << endl;
 
 int arlength, blength, wsrlength, strlength; // Relevant variables
 int printl;
 
 cin >> arlength;

 blength = 2 * arlength - 1;
 wsrlength = arlength - 1;
 strlength = 1;
 printl = 0; // We've printed no line yet
 
 for (printl = 0; printl <= ceil(blength / 2); printl++)
 { 
	strlength = 2 * ( printl ) + 1;
	for (j = 0;j < wsrlength;j++){
	     cout << "  ";
	}
	for (int k = 0;k < strlength;k++){
	     cout << "* ";
	}
	for (int l = 0;l < wsrlength;l++){
	     cout << "  ";
	}
	cout << endl;
	if (wsrlength > 0)
	    wsrlength--;
 }

 int d = ( (blength / 3) % 2 == 0 ? blength / 3 - 1 : blength / 3);
 
 wsrlength = blength / 2;
 
 for (int m = 0; m < 2 * arlength; m++)
 { 
	for (j = 0; j < arlength - 1 ; j++){
	     cout << "  ";
	}
	for (int k = j;k < j + d ;k++){
	     cout << "* ";
	}
	for (int l = j + d;l < 2 * arlength;l++){
	     cout << "  ";
	}
	cout << endl;
	if (wsrlength > 0)
	    wsrlength--;
 }
 
 return 0;
}
