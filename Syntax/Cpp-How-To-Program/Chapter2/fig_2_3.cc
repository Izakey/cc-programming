// Name: Welcome.cc
// Purpose: To welcome a programmer to C++
// Author: Isaac Kamga
// C++ How to Program, 8th Edition by Deitel & Deitel

#include <iostream> // Preprocessor directive to include the input/output stream header
using namespace std; // using directive to use any name in the standard library

int main (void)
{
	cout << "Hello World! Welcome to C++" << endl;
	return 0;
}
